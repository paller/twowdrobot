*[Kattints ide a magyar bevezetőhöz!](README.md)*

![2wdrobot](images/2wdrobot.jpg)

# Introduction

This project is about the control software of a 2-wheeled robot. The hardware itself is available from multiple sources at low cost. The aim is to create a software library that can
be easily used to implement various robot applications.

[Here is a short video about the robot (this is the twowdrobot_turn example program)](https://www.youtube.com/watch?v=XNFh8IUSGUQ) The robot was assembled by my son, Boldizsár Paller.

[Read the blog about the robot!](http://littlechineserobot.blogspot.com/)

[The project has been inspired by the Budapest Makers' Meetup](https://www.meetup.com/Budapest-Makers-Meetup/)


# Parts

* 2-wheeled robot chassis and motors (useful search expression on aliexpress.com: "2WD robot")
* Arduino Uno
* Arduino Sensor Shield v5.0 (not mandatory, makes the connection of the cables easier)
* L298N motor controller (useful search expression on aliexpress.com: "L298N motor driver board")
* MPU-6050 gyroscope board (usually sold as GY-521).
* Optical speed measurement sensor (useful search expression on aliexpress.com-on: "Optical speed measuring sensor")


For example [this kit](https://www.aliexpress.com/item/New-Avoidance-tracking-Motor-Smart-Robot-Car-Chassis-Kit-Speed-Encoder-Battery-Box-2WD-Ultrasonic-module/32587999240.html) 
contains everything but the gyroscope and the speed sensor from the list above. [One source of the gyro is this](https://www.aliexpress.com/item/Electronic-Board-6DOF-MPU6050-Module-Gyroscope-And-3-for-Axis-Accelerometer-Sensor-Module-For-Arduino-3/32605525405.html) and
[one source of the speed measurement sensor is this](https://www.aliexpress.com/item/1PCS-Speed-Measuring-Sensor-Counter-Motor-Test-Module-Groove-Type-Optical-Coupling-Module/32788770709.html)  
(hyperlinks were valid on 2017 April 11). 

# Robot assembly

[Assembly instructions](assembly_en.md)


# Installation

* Open the [project source file page](https://gitlab.com/paller/twowdrobot/tree/master).
* Download the sources in ZIP format with the download icon in the right upper corner.
* Unpack the ZIP sources into a directory.
* From the unzipped sources, choose the directory called  *libraries* and copy into your Arduino directory.
* Do Include library/TwoWDRobot in your Arduino environment to add the library to your sketch.

# Calibration

DC motors can behave quite differently. The software suite contains a calibration application that discovers the characteristics of both DC motors and saves the result into the EEPROM
of the microcontroller. 

Usage:

* Upload the [twowdrobot_calib](https://gitlab.com/paller/twowdrobot/tree/master/libraries/TwoWDRobot/examples/twowdrobot_calib) sketch into the Arduino Uno.
* Place the robot onto a larger, unobstructed surface. During the calibration the robot will move around, it needs about 1 sq meter of unobstructed place.
* Switch on the robot. After a delay of about 20-30 seconds, the robot will start turning around one of its wheels with increasing speed, then it repeats the same movement around the other wheel.
* Once the robot stops, you can switch it off and upload other sketches. The calibration data has already been saved into the Arduino Uno EEPROM so that the robot library can use it.

# Example programs

The project contains some example programs that can be accessed in the Arduino environment under File/Examples/TwoWDRobot menu item. 

* *robot_motor_test3* Basic motor test after robot assembly, spins up and down both motors. You can check whether the motors spin in the same direction and whether both motors are functional. 
* *twowdrobot_motors* Basic motor test, motors are switched into forward/backward mode. 
* *twowdrobot_gyro* Gyroscope test, displays the robot position angles on the serial output. 
* *twowdrobot_proximity* Test of the proximity sensor and the servo rotating that sensor. Turns the servo in 90 degree increments and displays the proximity values on the serial port.
* *twowdrobot_drive* Test of the gyroscope-controlled motor correction, the robot moves forward-backward.
* *twowdrobot_wheel* Tests the speed sensor, switches on the left motor then displays what the speed sensor measured.
* *twowdrobot_calib* Motor calibration application.
* *twowdrobot_turn* Test of the gyroscope-controlled motor correction and turning, the robot goes forward then turns 90 degrees to the right. Also turns if the proximity sensor detects obstacle.
  Use it after calibration.
* *twowdrobot_scan* Test of the ultrasonic ranger, rotates the ultrasonic sensor, measures the distances and the results are displayed on the serial port.
* *twowdrobot_patrol* Simple robot navigation controlled by the proximity sensor.

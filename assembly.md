# Az alváz összeszerelése

A következő képek mutatják be az alváz összeszerelését.

![chassis1](images/chassis1.jpg)

![chassis1](images/chassis2.jpg)

![chassis1](images/chassis3.jpg)

A két kerék mintázatának a képen látható irányba kell mutatnia. Megesik, hogy a kerekek nem párban érkeznek, ekkor a gumi leszedhető és megfordítható.

![chassis1](images/chassis4.jpg)

A szervót rögzítő alkatrész nem passzol a foglalatába.

![chassis1](images/chassis5.jpg)

Csípőfogóval vágtam méretre. Ha jobb megoldásod van, oszd meg!

![chassis1](images/chassis6.jpg)


![chassis1](images/chassis7.jpg)

Az összeszerelt szevó/közelségérzékelő a robotra szerelve.

![chassis1](images/chassis8.jpg)


# Elektromos alkatrészek összekötése

Az alvázra a következő alkatrészeket kell felszerelni.

* Arduino Uno
* Motorvezérlő
* Giroszkóp
* Sebességmérő szenzor

A giroszkóp ideális helye a két kerék között van, az irányának meg kell felelnie az alábbi képen láthatónak, a helye viszont szabadon megválasztható (alváz felett vagy alatt). A giroszkóp 
kártyán látható tengely-megjelöléseknek (ábrán bekarikázva) felfelé és a robot eleje felé kell nézniük.

![Giroszkóp irány](images/gyro_position.jpg)

A sebességmérő szenzort úgy kell felszerelni, hogy a kerék kódtárcsája megfelelően takarja-engedje át a szenzor fényét viszont a szenzor ne akadályozza a kerék forgását. A szenzor csak
az egyik kerék sebességét méri, mivel egyenes vonalú haladásnál használjuk csak. A [twowdrobot_wheel](https://gitlab.com/paller/twowdrobot/tree/master/libraries/TwoWDRobot/examples/twowdrobot_wheel) 
tesztprogram feltételezi, hogy ez a kerék a bal kerék (1-es motor), de egyébként mindegy, melyik kerék forgását méri a szenzor.

![Sebességmérő1](images/speed1.jpg)

![Sebességmérő2](images/speed2.jpg)


A hivatkozott készlet tartalmaz egy "Arduino Sensor Shield v5.0" nevű alkatrészt. Ez a kártya opcionális, csupán könnyebb hozzáférést biztosít az Arduino Uno kivezetéseihez. Amennyiben a készlet
tartalmazza, érdemes az Arduino Uno tetejére kapcsolni.

A következő összekötéseket kell létrehozni.

## Elemtartó

Legalább 7V-os elemcsomagra van szükség. A robotcsomagokkal általában 4 db AA-elemet befogadó elemtartó jön, de a szokásos NiMh akkumulátorokból 4 db (4x1.2V) nem ad elegendő tápfeszültséget.
Én egy 2db-os elemtartót használok 14500-as típusjelű Li-Ion elemekkel, melyek szintén AA formátumúak, de 3.7V-osak.

|Elemtartó		| Arduino Uno				|
|---------------------- | -----------				|
|Elemtartó, - kivezetés | Arduino Uno tápcsatlakozó, külső láb. |
|Elemtartó, + kivezetés | Arduino Uno tápcsatlakozó, belső láb. |

## Motorvezérlő

| Motorvezérlő	| Arduino Uno 		|
| ------------  | ----------- 		|
|  GND		| GND         		|
| +5V		| Vin (__NEM__ +5V) 	|
| ENA		| Pin 5			|
| ENB		| Pin 6			|
| IN1		| Pin 8			|
| IN2		| Pin 9			|
| IN3		| Pin 10		|
| IN4		| Pin 11		|

| Motorvezérlő  | Motorok		|
| ------------  | ----------- 		|
| OUT1		| Bal motor/1		|
| OUT2		| Bal motor/2		|
| OUT3		| Jobb motor/1		|
| OUT4		| Jobb motor/2		|

Ha ezzel megvagy, töltsük be a [robot_motor_test3](https://gitlab.com/paller/twowdrobot/tree/master/libraries/TwoWDRobot/examples/robot_motor_test3) tesztprogramot az Arduino Uno-ba
az USB kábelen keresztül. Kapcsoljuk le az USB kábelt, emeljük fel a robotot, kapcsoljuk be az elem kapcsolóját és azt kell látnunk, hogy mindkét kerék egyre növekvő, majd
egyre lassuló sebességgel előrefelé forog. Ha valamelyik kerék hátrafelé forogna, cseréljük fel a megfelelő OUT vezetékeket a motorvezérlőnél.

## Giroszkóp

| Giroszkóp	| Arduino Uno 		|
| ------------  | ----------- 		|
| GND		| GND			|
| VCC		| +5V			|
| SCL		| A5/SCL		|
| SDA		| A4/SDA		|
| INT		| Pin 3			|

## Szervó

A szervó vezetékei az én készletemben már fel voltak szerelve a szervóra. A következő színkód volt érvényes:

| Szervó	| Arduino Uno 		|
| ------------  | ----------- 		|
| Barna		| GND			|
| Piros		| +5V			|
| Sárga		| Pin 12		|

## Közelségérzékelő

| Közelségérzékelő	| Arduino Uno 		|
| ----------------  	| ----------- 		|
| GND			| GND			|
| Vcc			| +5V			|
| Trig			| Pin 7			|
| Echo			| Pin 4			|

Töltsd be a [twowdrobot_proximity](https://gitlab.com/paller/twowdrobot/tree/master/libraries/TwoWDRobot/examples/twowdrobot_proximity) tesztprogramot az Arduino Uno-ba
az USB kábelen keresztül és kapcsolj egy soros adaptert az Uno Tx lábához. Az USB kábel csatlakoztatása után (USB képes ezt a funkciót elegendő árammal ellátni) 
a szervónak 0, 90 és 180 fokos szögbe kell fordulnia kb. 5 mp-es időnként és a soros terminálon a közelségérzékelő által mért távolság látható.

## Sebességmérő szenzor

| Sebességmérő szenzor 		| Arduino Uno 		|
| --------------------  	| ----------- 		|
| GND				| GND			|
| VCC				| +5V			|
| DO				| Pin 2			|

Töltsd be a [twowdrobot_wheel](https://gitlab.com/paller/twowdrobot/tree/master/libraries/TwoWDRobot/examples/twowdrobot_wheel) teszt programot az Arduino Uno-ba
az USB kábelen keresztül és csatlakoztassunk egy soros adaptert az Uno Tx lábához. Kapcsold le az USB kábelt, emeljük fel a robotot, kapcsoljuk be az elem kapcsolóját
és nézd meg, mi jelent meg a soros terminálon. Először a sebességmérő szenzort álló motorral teszteli a program. Az eredménynek 0-nak kell lennie. Aztán
a bal kerék teljes sebességgel forog 5 másodpercig és a szenzor által számolt impulzusokat kijelzi a program. Az eredménynek 600-750 között kell lennie függően
a motor tulajdonságaitól.

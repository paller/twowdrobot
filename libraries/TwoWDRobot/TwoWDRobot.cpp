#include "Arduino.h"
#include "TwoWDRobot.h"
#include <Wire.h>

// I2Cdev and MPU6050 must be installed as libraries, or else the .cpp/.h files
// for both classes must be in the include path of your project
#include "I2Cdev.h"

#include "MPU6050_6Axis_MotionApps20.h"
//#include "MPU6050.h" // not necessary if using MotionApps include file

// Arduino Wire library is required if I2Cdev I2CDEV_ARDUINO_WIRE implementation
// is used in I2Cdev.h. This is the case for TwoWDRobot
#if I2CDEV_IMPLEMENTATION == I2CDEV_ARDUINO_WIRE
    #include <Wire.h>
#endif

// class default I2C address is 0x68
// specific I2C addresses may be passed as a parameter here
// AD0 low = 0x68 (default for SparkFun breakout and InvenSense evaluation board)
// AD0 high = 0x69
MPU6050 mpu;
//MPU6050 mpu(0x69); // <-- use for AD0 high


// Motor pins
// Motor 1 (left)
#define MOTOR1_DIR1	8
#define MOTOR1_DIR2	9
#define MOTOR1_SPEED	5	// Needs to be a PWM pin to be able to control motor speed

// Motor 2 (right)
#define MOTOR2_DIR1	10
#define MOTOR2_DIR2	11
#define MOTOR2_SPEED	6	// Needs to be a PWM pin to be able to control motor speed

// Proximity detector pins
#define PROXIMITY_ECHO		4 // Echo Pin
#define PROXIMITY_TRIGGER	7 // Trigger Pin

#define PROXIMITY_MAXIMUMRANGE	100	// Maximum range needed
#define PROXIMITY_MINIMUMRANGE	0	// Minimum range needed

// Proximity servo pin
#define PROXIMITY_SERVO		12

// LED13
#define LED13			13

// MPU6050
#define MPU6050_INTERRUPT_PIN 3  // INT pin of the MPU6050 is connected to this pin

// Wheel counter
#define WHEELCOUNTER_INTERRUPT_PIN	2	// Pin for the wheel code disk counter circuit

static int sign(int x) {
	if (x > 0) return 1;
	if (x < 0) return -1;
	return 0;
}

volatile bool mpuInterrupt = false;     // indicates whether MPU interrupt pin has gone high
void dmpDataReady() {
    mpuInterrupt = true;
}

volatile unsigned long wheelCounter = 0;
void increaseWheelCounter() {
	++wheelCounter;
}


TwoWDRobot::TwoWDRobot()
{
}

int TwoWDRobot::init() {
	Serial.begin(115200);
	init_proximity();
	init_proximity_servo();
	init_wheel_counter();
	init_motors();
	pinMode(LED13,OUTPUT);
	// Gyro/accel initialisation
	init_gyroaccel();
	return TWOWDROBOT_OK;
}

void TwoWDRobot::led13On() {
	digitalWrite(LED13, HIGH );
	
}

void TwoWDRobot::led13Off() {
	digitalWrite(LED13, LOW );
	
}

void TwoWDRobot::read_EEPROM(int base_addr,int len,uint8_t *dest) {
  uint8_t v;
  uint8_t *p = dest;
  for(int offs = 0 ; offs < len ; ++offs ) {
     v = EEPROM.read( base_addr+offs );
     *(p++) = v;
  }
}


// The sensor should be motionless on a horizontal surface 
//  while calibration is happening
void TwoWDRobot::calibrate_gyro( int16_t *gx_offset, int16_t *gy_offset, int16_t *gz_offset ) {
    int num_iter = 100;
    int32_t gx_sum = 0, gy_sum = 0, gz_sum = 0;
    int16_t  ax,ay,az,gx,gy,gz;
    for( int i = 0 ; i < num_iter ; ++i ) {
       mpu.getMotion6( &ax, &ay, &az, &gx, &gy, &gz);
       gx_sum += (int32_t)gx;
       gy_sum += (int32_t)gy;
       gz_sum += (int32_t)gz;
       delay(10);
    }
    gx_sum /= num_iter;
    gy_sum /= num_iter;
    gz_sum /= num_iter;
    *gx_offset = gx_sum;
    *gy_offset = gy_sum;
    *gz_offset = gz_sum;	
}

void TwoWDRobot::init_gyroaccel() {
	int16_t gx_offset,gy_offset,gz_offset;
	uint8_t	devStatus;
	uint8_t	mpuIntStatus;
	unsigned long	currentTime;
	
	// join I2C bus (I2Cdev library doesn't do this automatically)
#if I2CDEV_IMPLEMENTATION == I2CDEV_ARDUINO_WIRE
	Wire.begin();
	Wire.setClock(400000); // 100kHz I2C clock. Comment this line if having compilation difficulties
#elif I2CDEV_IMPLEMENTATION == I2CDEV_BUILTIN_FASTWIRE
	Fastwire::setup(400, true);
#endif
	mpu.initialize();
	mpu.setDLPFMode( MPU6050_DLPF_BW_98 );
	mpu.setRate( 19 );		// 100 Hz gyro sampling rate
	pinMode(MPU6050_INTERRUPT_PIN, INPUT);
	Serial.println(
		mpu.testConnection() ? 
		F("MPU6050 connection successful") : 
		F("MPU6050 connection failed"));
	
	//Initialize the angles
	calibrate_gyro( &gx_offset,&gy_offset,&gz_offset);  
	mpu.setXGyroOffset(gx_offset);
	mpu.setYGyroOffset(gy_offset);
	mpu.setZGyroOffset(gz_offset);
	mpu.setZAccelOffset(1788); // 1688 factory default for my test chip
	// load and configure the DMP
	Serial.println(F("Initializing DMP..."));
	devStatus = mpu.dmpInitialize();
	
	// make sure it worked (returns 0 if so)
	if (devStatus == 0) {
		// turn on the DMP, now that it's ready
		Serial.println(F("Enabling DMP..."));
		mpu.setDMPEnabled(true);
		
		attachInterrupt(digitalPinToInterrupt(MPU6050_INTERRUPT_PIN), dmpDataReady, RISING);
		mpuIntStatus = mpu.getIntStatus();
		
		// set our DMP Ready flag so the main loop() function knows it's okay to use it
		Serial.println(F("DMP ready! Waiting for first interrupt..."));
		dmpReady = true;
		
		// get expected DMP packet size for later comparison
		packetSize = mpu.dmpGetFIFOPacketSize();
	} else {
		// ERROR!
		// 1 = initial memory load failed
		// 2 = DMP configuration updates failed
		// (if it's going to break, usually the code will be 1)
		Serial.print(F("DMP Initialization failed (code "));
		Serial.print(devStatus);
		Serial.println(F(")"));
		dmpReady = false;
	}
	currentTime = millis();
	do {
		mpuIntStatus = mpu.getIntStatus();
		if( mpuIntStatus & 0x01 )
			break;
	} while( ( millis() - currentTime ) < 10000L );
	if( ( mpuIntStatus & 0x01 ) == 0 ) {
		Serial.println( F("No MPU interrupt received within 10 secs, MPU not functional" ));
		dmpReady = false;
	}
	gyro_last_timestamp = 0L;
	if( dmpReady ) {
		Serial.println( F("Gyro initialization done") );
		update_angles();
		Serial.println( F("First gyro update done") );
	}
}

bool TwoWDRobot::update_angles() {
	uint8_t fifoBuffer[64]; // FIFO storage buffer
	uint8_t	mpuIntStatus;
	uint16_t fifoCount;     // count of all bytes currently in FIFO

#ifdef LIFE_SIGNAL2
	Serial.print(F("."));
#endif
	bool rv = false;
	// if programming failed, don't try to do anything
	if (!dmpReady) return false;
	if( !mpuInterrupt ) return false;
	
// reset interrupt flag and get INT_STATUS byte
	mpuInterrupt = false;	
	mpuIntStatus = mpu.getIntStatus();
	
	// check for overflow (this should never happen unless our code is too inefficient)
	if ( mpuIntStatus & 0x10 ) {
		// reset so we can continue cleanly
		mpu.resetFIFO();
		Serial.print(F("FIFO overflow, int status: 0x"));
		Serial.println( mpuIntStatus, HEX );
		// otherwise, check for DMP data ready interrupt (this should happen frequently)
	} else if (mpuIntStatus & 0x02) {
		fifoCount = mpu.getFIFOCount();
		if( fifoCount >= packetSize ) {
			if( fifoCount >= 1024 ) {
				mpu.resetFIFO();
				Serial.print(F("FIFO overflow, FIFO count: "));
				Serial.println( fifoCount );
			} else {
				do {
					// read a packet from FIFO
					mpu.getFIFOBytes(fifoBuffer, packetSize);
					
					// track FIFO count here in case there is > 1 packet available
					// (this lets us immediately read more without waiting for an interrupt)
					fifoCount -= packetSize;
					
					// display Euler angles in degrees
					mpu.dmpGetQuaternion(&q, fifoBuffer);
					mpu.dmpGetGravity(&gravity, &q);
					mpu.dmpGetYawPitchRoll(ypr, &q, &gravity);
					for( int i = 0 ; i < 3 ; ++i )
						ypr[i] = ( ypr[i] * 180.0) /M_PI;
					gyro_last_timestamp = millis();
				} while( fifoCount >= packetSize );
				rv = true;
			}
		}
	} 
#ifdef LIFE_SIGNAL2
	Serial.print(F("*"));
#endif
	return rv; 
}

float TwoWDRobot::reset_fifo() {
	float lastYaw;
	
	mpu.getIntStatus();
	mpu.resetFIFO();
	for( int i = 0 ; i < 5 ; ++i ) {
		while( !update_angles() );
		lastYaw = gyro_get_yaw();
	}
	return lastYaw;	
}


void TwoWDRobot::gyro_delay( long msec ) {
#ifdef DEBUG
	last_motor_debug_msg_timestamp = 0;
	Serial.println( F("gyro_delay") );
#endif
	unsigned long ts = millis();
	while( ( millis() - ts ) < msec ) {
		update_angles();
#ifdef DEBUG
		long currentts = millis();
		if( ( last_motor_debug_msg_timestamp == 0L ) ||
			( ( currentts - last_motor_debug_msg_timestamp ) >= 100L ) ) {
			last_motor_debug_msg_timestamp = currentts;
			Serial.println( gyro_get_yaw() );
		}
#endif

	}  
}


void TwoWDRobot::init_proximity() {
	pinMode(PROXIMITY_TRIGGER, OUTPUT);
	pinMode(PROXIMITY_ECHO, INPUT);
}

long TwoWDRobot::get_distance() {
	long duration, distance; // Duration used to calculate distance

	digitalWrite(PROXIMITY_TRIGGER, LOW); 
	delayMicroseconds(2); 

	digitalWrite(PROXIMITY_TRIGGER, HIGH);
	delayMicroseconds(10); 
 
	digitalWrite(PROXIMITY_TRIGGER, LOW);
	duration = pulseIn(PROXIMITY_ECHO, HIGH);
 
//Calculate the distance (in cm) based on the speed of sound.
	distance = duration/58.2;
 
	if ( ( distance >= PROXIMITY_MAXIMUMRANGE ) || ( distance <= PROXIMITY_MINIMUMRANGE) )
		distance = -1L;
	return distance;
}

void TwoWDRobot::init_proximity_servo() {
	proximityServo.attach(PROXIMITY_SERVO);
	proximityServo.write(90);
	delay(1000);
}

void TwoWDRobot::set_distance_direction(int degree) {
	proximityServo.write(degree);
	delay(15);
}

void TwoWDRobot::init_wheel_counter() {
	reset_wheel_counter();
	attachInterrupt(digitalPinToInterrupt(WHEELCOUNTER_INTERRUPT_PIN), increaseWheelCounter, RISING);
}

unsigned long TwoWDRobot::get_wheel_counter() {
	noInterrupts();
	unsigned long v = wheelCounter;
	interrupts();
	return v;
}

void TwoWDRobot::reset_wheel_counter() {
	noInterrupts();
	wheelCounter = 0L;
	interrupts();
}



void TwoWDRobot::dump_calib_table() {
  Serial.println( F("--- Left motor ---") );
  for( int i = 0 ; i < MOTOR_CALIB_1_LEN ; ++i ) {
    if( motor1_calib[i].pwm_value == 0xFF )
        break;
    Serial.print( F("PWM: ") );
    Serial.print( motor1_calib[i].pwm_value );
    Serial.print( F("; ") ); 
    Serial.print( motor1_calib[i].power_percentage );
    Serial.println( F("%") );
  }
  Serial.println( F("--- Left motor end ---") );
  Serial.println( F("--- Right motor ---") );
  for( int i = 0 ; i < MOTOR_CALIB_2_LEN ; ++i ) {
    if( motor2_calib[i].pwm_value == 0xFF )
        break;
    Serial.print( F("PWM: ") );
    Serial.print( motor2_calib[i].pwm_value );
    Serial.print( F("; ") ); 
    Serial.print( motor2_calib[i].power_percentage );
    Serial.println( F("%") );
  }
  Serial.println( F("--- Right motor end ---") );
}



void TwoWDRobot::init_motors() {
// Program motor driver pins
  pinMode( MOTOR1_DIR1,OUTPUT);
  pinMode( MOTOR1_DIR2,OUTPUT);
  pinMode( MOTOR1_SPEED,OUTPUT);
  pinMode( MOTOR2_DIR1,OUTPUT);
  pinMode( MOTOR2_DIR2,OUTPUT);
  pinMode( MOTOR2_SPEED,OUTPUT);
// Obtain motor calibration info
  read_EEPROM(MOTOR_CALIB_1_BASE,MOTOR_CALIB_1_LEN,(uint8_t*)&motor1_calib);
  read_EEPROM(MOTOR_CALIB_2_BASE,MOTOR_CALIB_2_LEN,(uint8_t*)&motor2_calib);
  dump_calib_table();
  reset_pid_memory();
}

void TwoWDRobot::reset_pid_memory() {
// Init the PID controller data structures
  for( int i = 0 ; i < PID_MEMORY_LEN ; ++i )
	  pid_memory[i] = 0.0;
  pid_memory_ptr = 0;
}

int TwoWDRobot::get_pwm_from_power_percentage( bool left_motor, int pwr_percentage ) {
	motor_calib_entry	*p,*base;
	int		i,sign_multiplier,abs_pwr_percentage;
	uint8_t		prev_pwm,prev_pwr_percentage;
	bool		found;
	
	sign_multiplier = pwr_percentage < 0 ? -1 : 1;
	abs_pwr_percentage = abs( pwr_percentage );
	if( left_motor )
		base = motor1_calib;
	else
		base = motor2_calib;
	p = base;
// Follow a very dumb strategy if there's no calib data 
	if( p->pwm_value == 0xFF )
		return abs_pwr_percentage < 60 ? 0 : sign_multiplier*255;
	found = false;
	for( i = 0 ; i < MOTOR_CALIB_1_LEN ; ++i ) {
		if( p->pwm_value == 0xFF )
			break;		// Table end, no match
		if( p->power_percentage >= abs_pwr_percentage ) {
			found = true;
			break;
		}
		++p;
	}
	if( !found )
		return sign_multiplier*255;	// Max PWM if requested power percentage is greater than anything in the table
// Exact match
	if( p->power_percentage == abs_pwr_percentage )
		return sign_multiplier * p->pwm_value;
// Else choose the entry which is closer to the required power percentage. This function is executed very frequently during drive or turn actions,
// hence we don't have the luxury of proper interpolation
	if( i == 0 ) {
		prev_pwm = 0;
		prev_pwr_percentage = 0;
	} else {
		prev_pwm = base[i-1].pwm_value;
		prev_pwr_percentage = base[i-1].power_percentage;
	}
	return sign_multiplier * (
			abs( abs_pwr_percentage - prev_pwr_percentage ) < abs( p->power_percentage - abs_pwr_percentage ) ?
			prev_pwm :
			p->pwm_value );
}


void TwoWDRobot::motor1_forward(int speed) {
  digitalWrite( MOTOR1_DIR1, LOW);
  digitalWrite( MOTOR1_DIR2, HIGH);
  analogWrite( MOTOR1_SPEED, speed);//Sets speed variable via PWM 
}

void TwoWDRobot::motor1_backward(int speed) {
  digitalWrite( MOTOR1_DIR1, HIGH);
  digitalWrite( MOTOR1_DIR2, LOW);
  analogWrite( MOTOR1_SPEED, speed);//Sets speed variable via PWM 
}

void TwoWDRobot::motor1_stop() {
  digitalWrite( MOTOR1_DIR1, LOW);
  digitalWrite( MOTOR1_DIR2, LOW);
  analogWrite( MOTOR1_SPEED, 0);//Sets speed variable via PWM 
}


void TwoWDRobot::motor2_forward(int speed) {
  digitalWrite( MOTOR2_DIR1, LOW);
  digitalWrite( MOTOR2_DIR2, HIGH);
  analogWrite( MOTOR2_SPEED, speed);//Sets speed variable via PWM 
}

void TwoWDRobot::motor2_backward(int speed) {
  digitalWrite( MOTOR2_DIR1, HIGH);
  digitalWrite( MOTOR2_DIR2, LOW);
  analogWrite( MOTOR2_SPEED, speed);//Sets speed variable via PWM 
}

void TwoWDRobot::motor2_stop() {
  digitalWrite( MOTOR2_DIR1, LOW);
  digitalWrite( MOTOR2_DIR2, LOW);
  analogWrite( MOTOR2_SPEED, 0);//Sets speed variable via PWM 
}

void TwoWDRobot::motors_stop() {
	motor1_stop();
	motor2_stop();
}

void TwoWDRobot::motors_start( int speed1, int speed2 ) {
	if( speed2 < 0 )
		motor2_backward(abs(speed2)); 
	else
		motor2_forward(speed2); 

	if( speed1 < 0 )
		motor1_backward(abs(speed1)); 
	else
		motor1_forward(speed1); 
}


float TwoWDRobot::angle_diff( float a, float b ) {
	float diff = a - b;
	if( diff < -270.0 )
		return 360.0 + diff;
	else
	if( diff > 270.0 )
		return diff - 360.0;
	else
		return diff;
}

int TwoWDRobot::motors_run( int pwr1,int pwr2, int cbn, motors_run_callback cbs[] ) {
	int s_pwr1 = pwr1,s_pwr2 = pwr2;
	int prev_pwr1 = s_pwr1,prev_pwr2 = s_pwr2;
	int pwm1,pwm2;
	int rv;
	
	reset_pid_memory();
	motors_start( get_pwm_from_power_percentage( true,pwr1 ),
		      get_pwm_from_power_percentage ( false,pwr2 ) );
	unsigned long startts = millis();
#ifdef DEBUG
	last_motor_debug_msg_timestamp = 0L;
#endif
#ifdef LIFE_SIGNAL
	life_signal_timestamp = 0L;
#endif
	while( true ) {
		for( rv = 0 ; rv < cbn ; ++rv ) {
			motors_run_callback cbi = cbs[rv];
			if( cbi != NULL ) {
				int r = (*cbi)( this,startts, &s_pwr1, &s_pwr2 );
				switch( r ) {
					case MOTORS_RUN_QUIT:
						Serial.print( "MOTORS_RUN_QUIT: " );
						Serial.println( rv );
						goto l1;
					
					case MOTORS_RUN_CONTINUE:
						unsigned long currentts = millis();
// Make sure we don't update motor speeds too frequently. Minimum update period is 10 msec
						if( ( last_motor_control_timestamp == 0L ) || 
						    ( ( currentts - last_motor_control_timestamp ) >= 10L ) ) {
							last_motor_control_timestamp = currentts;
							pwm1 = abs( get_pwm_from_power_percentage( true,s_pwr1 ) );
							pwm2 = abs( get_pwm_from_power_percentage( false,s_pwr2 ) );
							if( sign(s_pwr1) != sign(prev_pwr1) )
								if( s_pwr1 > 0 )
									motor1_forward( pwm1 );
								else
									motor1_backward( pwm1 );
							else
								analogWrite( MOTOR1_SPEED, abs( pwm1 ) );
							if( sign(s_pwr2) != sign(prev_pwr2) )
								if( s_pwr2 > 0 )
									motor2_forward( pwm2 );
								else
									motor2_backward( pwm2 );
							else
								analogWrite( MOTOR2_SPEED,abs(  pwm2 ) );
							prev_pwr1 = s_pwr1;
							prev_pwr2 = s_pwr2;
						}
						break;
				}
			}
		}
	}
l1:	return rv;
}

int TwoWDRobot::motors_run_cb_timeout( TwoWDRobot *thisPtr,unsigned long startts, int *pwr1,int *pwr2 ) {
	unsigned long now = millis();
#ifdef LIFE_SIGNAL
	if( ( thisPtr->life_signal_timestamp == 0 ) || ( ( now - thisPtr->life_signal_timestamp ) >= 1000 ) ) {
		Serial.println( "Tick" );
		thisPtr->life_signal_timestamp = now;
	}
#endif
	if( ( now - startts ) >= thisPtr->run_timeout )
		return MOTORS_RUN_QUIT;
	else
		return MOTORS_RUN_CONTINUE;
}

int TwoWDRobot::motor_pid_controller( float error ) {
	float prev_error = pid_memory[pid_memory_ptr];
	pid_memory[pid_memory_ptr++] = error;
	pid_memory_ptr %= PID_MEMORY_LEN;
	float pid_out = error * 2.0;	// P part, K_p
	float ipart = 0.0;
	for( int i = 0 ; i < PID_MEMORY_LEN ; ++i )
		ipart += pid_memory[i];
//	ipart /= (float)PID_MEMORY_LEN;
	pid_out += ipart * 1.5;		// I part, K_i
	pid_out += ( error - prev_error ) * 6.0;	// D part, K_d
	return (int)pid_out;
}

int TwoWDRobot::motors_run_cb_adapt_direction( TwoWDRobot *thisPtr,unsigned long startts, int *pwr1,int *pwr2 ) {
	int full_pw,corrected_pw;
	
	if( thisPtr->update_angles() ) {
		float current_angle = thisPtr->gyro_get_yaw();
		float adiff =  angle_diff( current_angle, thisPtr->direction_to_keep );
#ifdef ERROR_MEASUREMENT
	unsigned long cts = millis();
	Serial.print( F("#ERR: ") );
	Serial.print( cts );
	Serial.print( F(","));
	Serial.println( adiff );
#endif
		int pwcorrection = thisPtr->motor_pid_controller( adiff );
		// Deviation to the right
		if( pwcorrection > 0 ) {
			full_pw = thisPtr->pwr_to_keep;
			corrected_pw = full_pw - pwcorrection;
			if( corrected_pw < 0 )
				corrected_pw = 0;
			// Going forward?
			if( thisPtr->pwr_to_keep > 0 ) {
				*pwr1 = corrected_pw;
				*pwr2 = full_pw;
				// Going backward
			} else {
				*pwr2 = corrected_pw;
				*pwr1 = full_pw;
			}
		} else
			// Deviation to the left
		{
			full_pw = thisPtr->pwr_to_keep;
			corrected_pw = full_pw + pwcorrection;
			if( corrected_pw < 0 )
				corrected_pw = 0;
			// Going forward?
			if( thisPtr->pwr_to_keep > 0 ) {
				*pwr2 = corrected_pw;
				*pwr1 = full_pw;
			} else {
				*pwr1 = corrected_pw;
				*pwr2 = full_pw;
			}
			// Going backward
		} 
#ifdef DEBUG
		unsigned long currentts = millis();
		if( ( thisPtr->last_motor_debug_msg_timestamp == 0L ) ||
			( ( currentts - thisPtr->last_motor_debug_msg_timestamp ) >= 100L ) ) {
			thisPtr->last_motor_debug_msg_timestamp = currentts;
			Serial.print( current_angle );
			Serial.print( F(",") );
			Serial.print( adiff );
			Serial.print( F(",") );
			Serial.print( *pwr1 );
			Serial.print( F(",") );
			Serial.println( *pwr2 );
		}
#endif
	}
	return MOTORS_RUN_CONTINUE;
}

int TwoWDRobot::motors_run_cb_bump( TwoWDRobot *thisPtr,unsigned long startts, int *pwr1,int *pwr2 ) {
	unsigned long currentts = millis();
	if( ( thisPtr->last_bump_check_timestamp == 0L ) || ( (  currentts - thisPtr->last_bump_check_timestamp ) >= 100L ) ) {
		thisPtr->last_bump_check_timestamp = currentts;
		for( int i = 0 ; i < 3 ; ++i ) {
			long d = thisPtr->get_distance();
			thisPtr->last_bump_check_timestamp = millis();
			if( ( d < 0 ) || ( d >= ((long)thisPtr->bump_distance) ) )
				return MOTORS_RUN_CONTINUE;
		}
		return MOTORS_RUN_QUIT;
	}
	return MOTORS_RUN_CONTINUE;
}

int TwoWDRobot::motors_run_cb_turn( TwoWDRobot *thisPtr,unsigned long startts, int *pwr1,int *pwr2 ) {
	int ret = MOTORS_RUN_CONTINUE;
	unsigned long cts;
	if( thisPtr->update_angles() ) {
		float cangle = thisPtr->gyro_get_yaw();
		float adiff = angle_diff( cangle, thisPtr->direction_to_reach );
#ifdef ERROR_MEASUREMENT
	cts = millis();
	Serial.print( F("#ERR: ") );
	Serial.print( cts );
	Serial.print( F(","));
	Serial.println( adiff );
#endif
		if( abs( adiff ) > 2.0 )
			thisPtr->last_dir_reached_timestamp = 0;
		else {
			cts = millis();
			if( ( thisPtr->last_dir_reached_timestamp != 0 ) &&
			    (  cts - thisPtr->last_dir_reached_timestamp ) >= 200 )
				ret = MOTORS_RUN_QUIT;
			if( thisPtr->last_dir_reached_timestamp == 0 )
				thisPtr->last_dir_reached_timestamp = cts;
		}
		int adapted_pwr = thisPtr->motor_pid_controller( adiff );
		if( abs(adapted_pwr) > thisPtr->pwr_to_keep )
			adapted_pwr = sign(adapted_pwr) * thisPtr->pwr_to_keep;
		*pwr2 = adapted_pwr;
		*pwr1 = -adapted_pwr;
#ifdef DEBUG
		unsigned long currentts = millis();
		if( ( thisPtr->last_motor_debug_msg_timestamp == 0L ) ||
			( ( currentts - thisPtr->last_motor_debug_msg_timestamp ) >= 100L ) ) {
			thisPtr->last_motor_debug_msg_timestamp = currentts;
			Serial.print( cangle );
			Serial.print( F(",") );
			Serial.print( adiff );
			Serial.print( F(",") );
			Serial.println( *pwr1 );
		}
#endif
	}
	return ret;
}


int TwoWDRobot::motors_run_cb_distance( TwoWDRobot *thisPtr,unsigned long startts, int *pwr1,int *pwr2 ) {
	unsigned long wc = thisPtr->get_wheel_counter();
	return wc >= thisPtr->wheel_counter_to_reach ? MOTORS_RUN_QUIT : MOTORS_RUN_CONTINUE;
}	


int TwoWDRobot::timed_straight_drive( unsigned long time_to_run, unsigned int distance_to_run, int pwr, int bd ) {
	motors_run_callback cbs[4];
	if( time_to_run == 0L )
		cbs[0] = NULL;
	else
		cbs[0] = &motors_run_cb_timeout;
	cbs[1] = &motors_run_cb_adapt_direction;
	if( bd == 0 )
		cbs[2] = NULL;
	else {
		cbs[2]= &motors_run_cb_bump;
		bump_distance = bd;
		last_bump_check_timestamp = 0L;
		set_distance_direction(90);	// ultrasonic sensor points straight ahead
	}
	if( distance_to_run == 0 )
		cbs[3] = NULL;
	else {
		wheel_counter_to_reach = (unsigned long)((float)distance_to_run * 3.5f);
		reset_wheel_counter();
		cbs[3] = &motors_run_cb_distance;
	}
		
	run_timeout = time_to_run;
	last_motor_control_timestamp = 0L;
	direction_to_keep = reset_fifo();
#ifdef DEBUG
	Serial.print( F("direction_to_keep:") );
	Serial.println( direction_to_keep );
#endif
	pwr_to_keep = pwr;
#ifdef ERROR_MEASUREMENT
	Serial.println( F("#START_STRAIGHT_DRIVE_ERROR_MEASUREMENT") );
#endif
	int rv = motors_run( pwr, pwr, 4, cbs );
	motors_stop();
#ifdef ERROR_MEASUREMENT
	Serial.println( F("#END_STRAIGHT_DRIVE_ERROR_MEASUREMENT") );
#endif
	return rv;
}

int TwoWDRobot::turn( float turn_by_degree, int pwr ) {
	int pwr1,pwr2;
	motors_run_callback cbs[2];

	// Whatever happens, finish after 5 secs
	run_timeout = 5000L;
	cbs[0] = &motors_run_cb_timeout;
	cbs[1] = &motors_run_cb_turn;

// Make sure, FIFO overruns are correctly cleaned up
	float current_angle = reset_fifo();
	direction_to_reach =  current_angle + turn_by_degree;
	last_dir_reached_timestamp = 0;
#ifdef DEBUG
	Serial.print( F("current_angle: ") );
	Serial.println( current_angle );
	Serial.print( F("direction_to_reach: ") );
	Serial.println( direction_to_reach );
#endif

	pwr_to_keep = pwr;
	turn_angle = turn_by_degree;
	last_motor_control_timestamp = 0L;
	if( turn_by_degree < 0.0 ) {
// Turning left, motor #1 backward, motor #2 forward
		pwr2 = pwr; 
		pwr1 = -pwr;
	} else {
// Turning right, motor #1 forward, motor #2 backward
		pwr1 = pwr;
		pwr2 = -pwr;
	}
#ifdef ERROR_MEASUREMENT
	Serial.println( F("#START_TURN_ERROR_MEASUREMENT") );
#endif
	int rv = motors_run( pwr1, pwr2, 2, cbs );
	motors_stop();
#ifdef ERROR_MEASUREMENT
	Serial.println( F("#END_TURN_ERROR_MEASUREMENT") );
#endif
	return rv;
}
#include <TwoWDRobot.h>

TwoWDRobot  robot;

float base_yaw;

void yaw_diff() {
  float new_yaw = robot.gyro_get_yaw();
  float diff = new_yaw - base_yaw;
  if( diff < 0 )
    diff = 360 + diff;
  Serial.print( F("Base yaw: ") );
  Serial.print( base_yaw );
  Serial.print( F("; new yaw: " ) );
  Serial.print( new_yaw );
  Serial.print( F("; diff: " ) );
  Serial.println( diff );
}

void setup() {
  robot.init();
  robot.led13On();
  Serial.println(F("Start"));
  Serial.println( F("Gyro warmup ... (10 secs)" ) );
  robot.led13Off();
  robot.gyro_delay( 10000L );
  Serial.println( F("Gyro warmup done" ) );
}

void loop() {
  while( true ) {
    base_yaw = robot.gyro_get_yaw();
    robot.timed_straight_drive( 10000, 100, 100, 20 );
    robot.gyro_delay( 2000L );
    yaw_diff();
    base_yaw = robot.gyro_get_yaw();
    robot.turn( -90.0,100 );
    robot.gyro_delay( 2000L );
    yaw_diff();
  }
}

#include  <TwoWDRobot.h>

TwoWDRobot  robot;

int speed;

void setup() {
    robot.init();
    speed = 200;
}

void loop() {
  robot.led13On();
  robot.motor1_forward( speed );
  delay( 2000 );
  robot.motor1_backward( speed );
  delay( 2000 );
  robot.motor1_stop();
  robot.led13Off();
  delay( 2000 );

  robot.led13On();
  robot.motor2_forward( speed );
  delay( 2000 );
  robot.motor2_backward( speed );
  delay( 2000 );
  robot.motor2_stop();
  robot.led13Off();
  delay( 2000 );
  
  robot.led13On();
  robot.motor1_backward( speed );
  robot.motor2_forward( speed );
  delay( 2000 );
  robot.motors_stop();  
  robot.led13Off();
  delay( 2000 );
}

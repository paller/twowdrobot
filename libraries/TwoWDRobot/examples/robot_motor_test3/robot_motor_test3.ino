// Motor 1
const int dir1PinA = 8;
const int dir2PinA = 9;
const int speedPinA = 5; // Needs to be a PWM pin to be able to control motor speed

// Motor 2
const int dir1PinB = 10;
const int dir2PinB = 11;
const int speedPinB = 6; // Needs to be a PWM pin to be able to control motor speed


const int ledPin = 13;

void setup() {
  pinMode(dir1PinA,OUTPUT);
  pinMode(dir2PinA,OUTPUT);
  pinMode(speedPinA,OUTPUT);
  pinMode(dir1PinB,OUTPUT);
  pinMode(dir2PinB,OUTPUT);
  pinMode(speedPinB,OUTPUT);
  pinMode(ledPin,OUTPUT);
}

void loop() {
  // this function will run the motors across the range of possible speeds
  // note that maximum speed is determined by the motor itself and the operating voltage
  // the PWM values sent by analogWrite() are fractions of the maximum speed possible 
  // by your hardware
  // turn on motors
  digitalWrite(dir1PinA, LOW);
  digitalWrite(dir2PinA, HIGH);  
  digitalWrite(dir1PinB, LOW);
  digitalWrite(dir2PinB, HIGH); 
  digitalWrite( ledPin,HIGH );
  // accelerate from zero to maximum speed
  for (int i = 0; i < 256; i++)
  {
    analogWrite(speedPinA, i);
    analogWrite(speedPinB, i);
    delay(20);
  } 
  digitalWrite( ledPin,LOW );
  // decelerate from maximum speed to zero
  for (int i = 255; i >= 0; --i)
  {
    analogWrite(speedPinA, i);
    analogWrite(speedPinB, i);
    delay(20);
  } 
  // now turn off motors
  digitalWrite(dir1PinA, LOW);
  digitalWrite(dir2PinA, LOW);  
  digitalWrite(dir1PinB, LOW);
  digitalWrite(dir2PinB, LOW);    
}

#include <TwoWDRobot.h>

TwoWDRobot  robot;

void scanForward( int dirs[], int steps) {
  int step_degree = 180 / ( steps - 1);
  int i,degree = 0,avgCtr;
  for( i = 0 ; i < steps ; ++i ) {
    robot.set_distance_direction(180-degree);
    delay(500);
    dirs[i] = -1;
    avgCtr = 0;
    long dist = robot.get_distance(); // Drop the first measurement as it is often bogus
    for( int n = 0 ; n < 3 ; ++n ) {
      dist = robot.get_distance();
      Serial.print( F("degree: "));
      Serial.print( 180-degree );
      Serial.print( F("; iter: " ));
      Serial.print( n );
      Serial.print( F("; dist: "));
      Serial.println( dist );
      if( dist >= 0 ) {
        if( dirs[i] >= 0 )
            dirs[i] += dist;
        else
            dirs[i] = dist;
        ++avgCtr;
      }
    }
    if( avgCtr > 1 )
        dirs[i] /= avgCtr;
    Serial.print( F("dirs["));
    Serial.print(i);
    Serial.print( F("]:"));
    Serial.println( dirs[i] );
    degree += step_degree;
  }
}

#define STEPS     19
int measured_dirs[STEPS];

void setup() {
  robot.init();
  scanForward( measured_dirs,STEPS );
}



void loop() {
  int step_degree = 180 / ( STEPS - 1);
  Serial.println( F("#START"));
  int deg = 0;
  for( int i = 0 ; i < STEPS ; ++i ) {
    Serial.print( F("#DIST:"));
    Serial.print( deg );
    Serial.print( F(","));
    Serial.println( measured_dirs[i] );
    deg += step_degree;    
  }
  Serial.println( F("#END"));
  delay(3000);
}


#include "TwoWDRobot.h"
#include "TwoWDRobot_EEPROM.h"
#include  <EEPROM.h>

TwoWDRobot  robot;
motor_calib_array motor1_calib;
motor_calib_array motor2_calib;

struct motor_calibpoint_s {
 uint8_t  motor_speed;
 float  rotation_angle;
};

typedef struct motor_calibpoint_s motor_calibpoint;
#define CALIBPOINTS 30
motor_calibpoint left_calibpoints[CALIBPOINTS];
motor_calibpoint right_calibpoints[CALIBPOINTS];

void read_EEPROM(int base_addr,int len,uint8_t *dest) {
  uint8_t v;
  uint8_t *p = dest;
  for(int offs = 0 ; offs < len ; ++offs ) {
     v = EEPROM.read( base_addr+offs );
     *(p++) = v;
  }
}

void write_EEPROM(int base_addr,int len,uint8_t *src) {
  uint8_t v;
  uint8_t *p = src;
  for(int offs = 0 ; offs < len ; ++offs ) {
     v = *(p++);
     EEPROM.write( base_addr+offs,v );
  }
}

float measure_rotation( bool left_motor,int motor_speed, long meas_len_ms ) {
  long currentts,outer_start_ts,inner_start_ts;
  float start_angle,acc_angle,current_angle,angle_diff;

  if( left_motor ) {
    robot.motor1_forward( motor_speed );
    robot.motor2_stop();
  } else {
    robot.motor2_forward( motor_speed );
    robot.motor1_stop();
  }
  // Eliminate FIFO overflows
  for( int i = 0 ; i < 3 ; ++i )
      robot.update_angles();  
  outer_start_ts = millis();
  acc_angle = 0.0f;
  while( ( ( currentts = millis() ) - outer_start_ts ) < meas_len_ms ) {
    start_angle = robot.gyro_get_yaw();
    inner_start_ts = currentts;
    // We assume there will not be a full rotation in 300 msec
    while( ( millis() - inner_start_ts ) < 300L ) {
      robot.update_angles();
      delay(1);  
    }
    current_angle = robot.gyro_get_yaw();
    angle_diff = current_angle - start_angle;
    if( ( start_angle > 90.0 ) && ( current_angle < -90.0 ) )
        angle_diff += 360.0;
    if( ( start_angle < -90.0 ) && ( current_angle > 90.0 ) )
        angle_diff -= 360.0;
    acc_angle += angle_diff;    
  }
  return acc_angle;
}

void measurement_cycle() {
  int meas_len_sec = 3,calibpoint_index;
  float rotation_angle,max_rotation_angle;
  for( int i = 0 ; i < CALIBPOINTS ; ++i ) {
    left_calibpoints[i].motor_speed = 0xFF; 
    right_calibpoints[i].motor_speed = 0xFF; 
  }
  max_rotation_angle = 0.0f;
  calibpoint_index = 0;
  Serial.println( "*** Left motor ***" );
  for( int motor_speed = 10 ; motor_speed < 255 ; motor_speed += 10 ) {
      rotation_angle = measure_rotation( true,motor_speed, meas_len_sec*1000L );
      Serial.print( F("motor speed: "));
      Serial.print( motor_speed );
      Serial.print( F("; rotation angle: " ));
      Serial.println( rotation_angle );
      if( calibpoint_index < CALIBPOINTS ) {
        left_calibpoints[calibpoint_index].motor_speed = motor_speed;
        left_calibpoints[calibpoint_index].rotation_angle = rotation_angle;
        ++calibpoint_index;
      }
  }
  robot.motors_stop();
  max_rotation_angle = 0.0f;
  calibpoint_index = 0;
  Serial.println( "*** Right motor ***" );
  for( int motor_speed = 10 ; motor_speed < 255 ; motor_speed += 10 ) {
      rotation_angle = abs( measure_rotation( false,motor_speed, meas_len_sec*1000L ) );
      Serial.print( F("motor speed: "));
      Serial.print( motor_speed );
      Serial.print( F("; rotation angle: " ));
      Serial.println( rotation_angle );
      if( calibpoint_index < CALIBPOINTS ) {
        right_calibpoints[calibpoint_index].motor_speed = motor_speed;
        right_calibpoints[calibpoint_index].rotation_angle = rotation_angle;
        ++calibpoint_index;
      }
  }  
  robot.motors_stop();
}

void dump_calib_table() {
  Serial.println( F("--- Left motor ---") );
  for( int i = 0 ; i < MOTOR_CALIB_1_LEN ; ++i ) {
    if( motor1_calib[i].pwm_value == 0xFF )
        break;
    Serial.print( F("PWM: ") );
    Serial.print( motor1_calib[i].pwm_value );
    Serial.print( F("; ") ); 
    Serial.print( motor1_calib[i].power_percentage );
    Serial.println( F("%") );
  }
  Serial.println( F("--- Left motor end ---") );
  Serial.println( F("--- Right motor ---") );
  for( int i = 0 ; i < MOTOR_CALIB_2_LEN ; ++i ) {
    if( motor2_calib[i].pwm_value == 0xFF )
        break;
    Serial.print( F("PWM: ") );
    Serial.print( motor2_calib[i].pwm_value );
    Serial.print( F("; ") ); 
    Serial.print( motor2_calib[i].power_percentage );
    Serial.println( F("%") );
  }
  Serial.println( F("--- Right motor end ---") );
}

void produce_calib_data() {
    int calib_idx;
    uint8_t calib_val;
    
// First look for the maximum rotation in both calibration point series
    float max_rotation = 0.0f;
    for( int i = 0 ; i < CALIBPOINTS ; ++i ) {
      if( left_calibpoints[i].motor_speed == 0xFF )
          break;
      if( left_calibpoints[i].rotation_angle > max_rotation )
          max_rotation = left_calibpoints[i].rotation_angle;
    }
    for( int i = 0 ; i < CALIBPOINTS ; ++i ) {
      if( right_calibpoints[i].motor_speed == 0xFF )
          break;
      if( right_calibpoints[i].rotation_angle > max_rotation )
          max_rotation = right_calibpoints[i].rotation_angle;
    }
    Serial.print( F("Maximum rotation: ") );
    Serial.println( max_rotation );

// Then scale the rotation values by the maximum rotation
    for( int i = 0 ; i < CALIBPOINTS ; ++i ) {
      if( left_calibpoints[i].motor_speed == 0xFF )
          break;
      left_calibpoints[i].rotation_angle /= max_rotation;
      left_calibpoints[i].rotation_angle *= 100.0;
    }
    for( int i = 0 ; i < CALIBPOINTS ; ++i ) {
      if( right_calibpoints[i].motor_speed == 0xFF )
          break;
      right_calibpoints[i].rotation_angle /= max_rotation;
      right_calibpoints[i].rotation_angle *= 100.0;
    }

// Turn the percentages into integers and fill the calibration table
    memset( (void*)motor1_calib,0xFF, MOTOR_CALIB_1_LEN );
    memset( (void*)motor2_calib,0xFF, MOTOR_CALIB_2_LEN );
    calib_idx = 0;
    for( int i = 0 ; i < CALIBPOINTS ; ++i ) {
      if( left_calibpoints[i].motor_speed == 0xFF )
          break;
      calib_val = (uint8_t)left_calibpoints[i].rotation_angle;
      if( calib_val == 0 )
        continue;
      motor1_calib[calib_idx].pwm_value = left_calibpoints[i].motor_speed;
      motor1_calib[calib_idx].power_percentage = calib_val;
      calib_idx++;
    }
    calib_idx = 0;
    for( int i = 0 ; i < CALIBPOINTS ; ++i ) {
      if( right_calibpoints[i].motor_speed == 0xFF )
          break;
      calib_val = (uint8_t)right_calibpoints[i].rotation_angle;
      if( calib_val == 0 )
        continue;
      motor2_calib[calib_idx].pwm_value = right_calibpoints[i].motor_speed;
      motor2_calib[calib_idx].power_percentage = calib_val;
      calib_idx++;
    }
}

void setup() {
  robot.init();
  Serial.println(F("Motor calibration start"));
  Serial.print( F("Reading ") );
  Serial.print( MOTOR_CALIB_1_LEN );
  Serial.print( F(" bytes fron EEPROM address 0x") );
  Serial.println( MOTOR_CALIB_1_BASE,HEX );
  read_EEPROM(MOTOR_CALIB_1_BASE,MOTOR_CALIB_1_LEN,(uint8_t*)&motor1_calib);
  Serial.print( F("Reading ") );
  Serial.print( MOTOR_CALIB_2_LEN );
  Serial.print( F(" bytes fron EEPROM address 0x") );
  Serial.println( MOTOR_CALIB_2_BASE,HEX );
  read_EEPROM(MOTOR_CALIB_2_BASE,MOTOR_CALIB_2_LEN,(uint8_t*)&motor2_calib);
  dump_calib_table();
  Serial.println( F("Gyro stabilization (10 secs)") );
  robot.led13On();
  long ts = millis();
  while( ( millis() - ts ) < 10000L ) {
      robot.update_angles();
      delay(1);
  }  
  robot.led13Off();
  Serial.println( F("Start measurement"));
  measurement_cycle();
  produce_calib_data();
  write_EEPROM( MOTOR_CALIB_1_BASE,MOTOR_CALIB_1_LEN,(uint8_t*)&motor1_calib );
  write_EEPROM( MOTOR_CALIB_2_BASE,MOTOR_CALIB_2_LEN,(uint8_t*)&motor2_calib );
}

void loop() {
  dump_calib_table();
  delay( 3000 );
}


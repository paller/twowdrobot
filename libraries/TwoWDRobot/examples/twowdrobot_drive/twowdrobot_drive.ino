#include <TwoWDRobot.h>

TwoWDRobot  robot;

int speed;

void setup() {
  robot.init();
  speed = 90;
  robot.gyro_delay( 10000L );
}

void loop() {
  robot.led13On();
  robot.timed_straight_drive( 10000, 50, speed, 10 );
  robot.led13Off();
  robot.gyro_delay( 2000L );
  robot.timed_straight_drive( 10000, 50, -speed,0 );
  robot.led13Off();
  robot.gyro_delay( 2000L );
}

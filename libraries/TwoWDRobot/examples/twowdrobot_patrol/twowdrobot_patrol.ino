#include <TwoWDRobot.h>

TwoWDRobot  robot;

void scanForward( int dirs[], int steps) {
  int step_degree = 180 / ( steps - 1);
  int i,degree = 0,avgCtr;
  for( i = 0 ; i < steps ; ++i ) {
    robot.set_distance_direction(180-degree);
    delay(500);
    dirs[i] = -1;
    avgCtr = 0;
    long dist = robot.get_distance();
    for( int n = 0 ; n < 3 ; ++n ) {
      dist = robot.get_distance();
      Serial.print( F("degree: "));
      Serial.print( 180-degree );
      Serial.print( F("; iter: " ));
      Serial.print( n );
      Serial.print( F("; dist: "));
      Serial.println( dist );
      if( dist >= 0 ) {
        if( dirs[i] >= 0 )
            dirs[i] += dist;
        else
            dirs[i] = dist;
        ++avgCtr;
      }
    }
    if( avgCtr > 1 )
        dirs[i] /= avgCtr;
    Serial.print( F("dirs["));
    Serial.print(i);
    Serial.print( F("]:"));
    Serial.println( dirs[i] );
    degree += step_degree;
  }
}

// 19 steps in 180 degrees, 10 degree step size
#define STEPS     19

// Direction codes for the next_dir method, dirs array
#define AHEAD     (STEPS-1)/2
#define MAX_DIST  1000    // Larger than anything possible measurement from the sensor

int min_neighbours( int dirs[], int pos, int steps, int neighbourhood ) {
  int minpos = pos - neighbourhood < 0 ? 0 : pos - neighbourhood;
  int maxpos = pos + neighbourhood >= steps ? steps - 1 : pos + neighbourhood;
  int v = MAX_DIST;
  for( int i = minpos ; i < maxpos ; ++i ) {
    int d = dirs[i];
    if( d >= 0 )
        v = min(v, d);
  }
  if( v == MAX_DIST )
      v = -1;
  return v;
}

float next_dir(int limit_distance) {
  int v,v_dirs[STEPS],orig_dirs[STEPS];
  bool dirs[STEPS];

  scanForward( orig_dirs,STEPS );
  Serial.println( F("#PATROL_SCAN"));
// The ultrasonic sensor is extremely unreliable if the obstacle and the sensor's direction is far from 90 degrees.
// Therefore we block 30 degrees (that means, three steps) from every obstacle detected
  int deg = 0;
  int steps_deg = 180 / ( STEPS - 1 );
  for( int i = 0 ; i < STEPS ; ++i ) {
    v = orig_dirs[i];
    Serial.print( F("#ORIG_DIST:"));
    Serial.print( deg );
    Serial.print( F(","));
    Serial.println( v );
    v_dirs[i] = min_neighbours( orig_dirs,i,STEPS,3 );
    Serial.print( F("#CORR_DIST:"));
    Serial.print( deg );
    Serial.print( F(","));
    Serial.println( v_dirs[i] );
    deg += steps_deg;
  }
  deg = 0;
  for( int i = 0 ; i < STEPS ; ++i ) {
    dirs[i] = ( v_dirs[i] >= 0 ) && ( v_dirs[i] < limit_distance );
    Serial.print( F("#BOOL_DIST:"));
    Serial.print( deg );
    Serial.print( F(","));
    Serial.println( dirs[i] );
    deg += steps_deg;
  }
  Serial.println( F("#END_PATROL_SCAN"));
      
// We prefer straight ahead
  if( !dirs[AHEAD] )
      return 0.0;
// Else turn left, preferably
  float dir = -90.0;
  for( int i = 0 ; i < STEPS ; ++i ) {
    if(!dirs[i])
      return dir;
    dir += steps_deg;
  }
// Turn back, where we came from
  return 180.0;
}

void setup() {
  robot.init();
  robot.led13On();
  Serial.println(F("Start"));
  Serial.println( F("Gyro warmup ... (10 secs)" ) );
  robot.gyro_delay( 10000L );
  Serial.println( F("Gyro warmup done" ) );
  robot.led13Off();
}

void loop() {
    int reason;
    int limit_distance = 80;
    
    float nd = next_dir(limit_distance);
    robot.set_distance_direction(90);
    Serial.print( F("nd: "));
    Serial.println( nd );
    if( nd != 0.0  ) {
        Serial.print( F("turn by: "));
        Serial.println( nd );
        reason = 0;
        while( true ) {
          reason = robot.turn( nd,100 );
          Serial.print(F("turn1, reason:"));
          Serial.println( reason );
          robot.gyro_delay( 1000L );
          if( reason == MOTOR_RUN_LIMITPARM )
            break;
 // Let's try to back off
          robot.timed_straight_drive( 2000, 20, -100, 0 );
          robot.gyro_delay( 1000L );
          nd = next_dir(limit_distance);
          robot.set_distance_direction(90);
          if( nd == 0.0 )
              break;
        }
    }
    Serial.println( F("straight_drive"));
    reason = robot.timed_straight_drive( 5000, 60, 100, 20 );
    Serial.print(F("straight1, reason:"));
    Serial.println( reason );
    if( reason == MOTOR_RUN_TIMEOUT )
          robot.timed_straight_drive( 2000, 20, -100, 0 );
    robot.gyro_delay( 1000L );
}


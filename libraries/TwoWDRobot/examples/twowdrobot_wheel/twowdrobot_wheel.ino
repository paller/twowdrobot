#include <TwoWDRobot.h>
#include <TwoWDRobot_EEPROM.h>

TwoWDRobot  robot;

volatile uint16_t  steps;
void wheelCoderInterrupt() {
    ++steps;
}

void setup() {
  robot.init();
  attachInterrupt(digitalPinToInterrupt(2), wheelCoderInterrupt, RISING);
}

void loop() {
  Serial.println( "Stop motors" );
  robot.motors_stop();
  steps = 0;
  Serial.println( "Waiting 5 secs to see if something moves" );
  delay(5000);
  Serial.print( "Step counter: ");
  Serial.println( steps );
  steps = 0;
  robot.motor1_forward( 255 );
  delay(5000);
  robot.motors_stop();
  Serial.print( "Step counter: ");
  Serial.println( steps );
  delay(1000);  
}

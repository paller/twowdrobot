#include "TwoWDRobot.h"

TwoWDRobot  robot;

void setup() {
  robot.init();
}

void loop() {
  // put your main code here, to run repeatedly:
  robot.update_angles();
  float yaw = robot.gyro_get_yaw();
  float pitch = robot.gyro_get_pitch();
  float roll = robot.gyro_get_roll();
  Serial.print( F("ypr: ") );
  Serial.print(yaw);
  Serial.print(F(","));
  Serial.print( pitch );
  Serial.print(F(","));
  Serial.print( roll );
  Serial.println(F(""));
  delay(1);
}

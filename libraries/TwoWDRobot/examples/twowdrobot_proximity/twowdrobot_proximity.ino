#include <TwoWDRobot.h>

TwoWDRobot  robot;

int loopCounter;
int directionAngle;

void setup() {
  robot.init();
  loopCounter = 0;
  directionAngle = 0;
}

void loop() {
  // put your main code here, to run repeatedly:
  long distance = robot.get_distance();
  if( distance < 0 )
    Serial.println( "No reading" );
    else {
      Serial.print( distance );
      Serial.println( " cm" );
    }
  ++loopCounter;
  if( ( loopCounter % 30 ) == 0 ) {
//    robot.set_distance_direction(directionAngle);
    directionAngle += 90;
    if( directionAngle > 180 )
      directionAngle = 0;
  }
  delay(100);
}


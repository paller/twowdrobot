/** @file
  @brief Library for controlling a 2WD robot system
  Written by Gabor Paller

Contains code from:
- MPU-6050 functions:
	Code based on Jeff Rowberg's I2Cdev and MPU6050 libraries. These libraries are also included into this source code bundle.
- Proximity sensor:
	This sketch originates from Virtualmix: http://goo.gl/kJ8Gl
	Has been modified by Winkle ink here: http://winkleink.blogspot.com.au/2012/05/arduino-hc-sr04-ultrasonic-distance.html
	And modified further by ScottC here: http://arduinobasics.blogspot.com.au/2012/11/arduinobasics-hc-sr04-ultrasonic-sensor.html


*/
#ifndef TwoWDRobot_h
#define TwoWDRobot_h

#include "Arduino.h"
#include <Servo.h>
#include <EEPROM.h>
#include "helper_3dmath.h"
#include "TwoWDRobot_EEPROM.h"

#define TWOWDROBOT_OK	0

#define MOTORS_RUN_QUIT			0
#define MOTORS_RUN_CONTINUE		1

#define	MOTOR_RUN_TIMEOUT	0
#define MOTOR_RUN_LIMITPARM	1
#define MOTOR_RUN_PROXIMITY	2
#define MOTOR_RUN_DISTANCE	3

//#define DEBUG	1
//#define	LIFE_SIGNAL	1
//#define 	LIFE_SIGNAL2	1
//#define ERROR_MEASUREMENT	1

#define PID_MEMORY_LEN	5


class TwoWDRobot;
// motors_run callback pointer
typedef int (*motors_run_callback)(TwoWDRobot *thisPtr, unsigned long startts, int *speed1,int *speed2);

static int sign(int x);

/** @class TwoWDRobot
 * @brief Holder for all the robot functions
 * This class is usually instantiated once.
 * TwoWDRobot robot;
 * ...
 * robot.imit();
 */
class TwoWDRobot
{
  public:
	TwoWDRobot();
	/**
	 * Initializes the instance and the underlying hardware elements.
	 * @return Always returns 0.
	 */
	int init();
	
// ----------------------------- LED13 --------------------------------------------
	/**
	 * Turns on the "pin 13" LED of the Arduino Uno
	 */
	void led13On();
	/**
	 * Turns off the "pin 13" LED of the Arduino Uno
	 */
	void led13Off();
	
// ----------------------------  EEPROM -------------------------------------------
	/**
	 * Read data fron EEPROM into the microcontroller's RAM memory
	 * @param base_addr Starting address in the EEPROM
	 * @param len Length of the data in the EEPROM
	 * @param dest Destination address in the RAM where the data in the EEPROM will be copied to 
	 */
	void read_EEPROM(int base_addr,int len,uint8_t *dest);

	
// ---------------------------- Gyro/accel section --------------------------------
	unsigned long gyro_get_last_time() { return gyro_last_timestamp; }
	/**
	 * Returns the yaw value of the robot chassis.
	 * @return the yaw value
	 */
	float gyro_get_yaw() {return ypr[0];}
	/**
	 * Returns the pitch value of the robot chassis.
	 * @return the pitch value
	 */
	float gyro_get_pitch() {return ypr[2];}
	/**
	 * Returns the roll value of the robot chassis.
	 * @return the roll value
	 */
	float gyro_get_roll() {return ypr[1];}
	void calibrate_gyro( int16_t *gx_offset, int16_t *gy_offset, int16_t *gz_offset );
	bool update_angles();
	/**
	 * Version of the delay function that empties the gyro's FIFO while waiting. If this function is used, there will be no FIFO overruns hence
	 * the roll-pitch-yaw values will be immediately correct.
	 * @param msec Wait timeout in msec.
	 */
	void gyro_delay( long msec );

// -------------------------- Proximity sensor ------------------------------------
	/**
	 * Measures the distance of an obstacle in the direction the proximity sensor is pointing.
	 * @return Distance of the obstacle in centimeters. Returns -1 if there is no obstacle detected.
	 */
	long get_distance();

// ------------------------- Wheel coder sensor -----------------------------------
	/**
	 * Reads the current value of the wheel rotation sensor counter. One full rotation of the wheel increases the counter by 20.
	 * @return The wheel rotation sensor's current value
	 */
	unsigned long get_wheel_counter();
	/**
	 * Resets the wheel rotation sensor counter to 0.
	 */
	void reset_wheel_counter();
	
// -------------------------- Proximity servo -------------------------------------
	/**
	 * Sets the direction of the proximity detector.
	 * @param degree Direction of the proximity sensor, in degrees. 0 points right, 90 forward, 180 to the left. 
	 */
	void set_distance_direction(int degree);

// -------------------------- Motors section --------------------------------------
	/**
	 * Low level motor control, sets the motor#1 (left wheel) to the specified speed in forward direction.
	 * @param speed Speed of the motor, PWM value (0-255).
	 */
	void motor1_forward(int speed);
	/**
	 * Low level motor control, sets the motor#1 (left wheel) to the specified speed in backward direction.
	 * @param speed Speed of the motor, PWM value (0-255).
	 */
	void motor1_backward(int speed);
	/**
	 * Low level motor control, stops motor#1 (left wheel).
	 * @param speed Speed of the motor, PWM value (0-255).
	 */
	void motor1_stop();
	/**
	 * Low level motor control, sets the motor#2 (right wheel) to the specified speed in forward direction.
	 * @param speed Speed of the motor, PWM value (0-255).
	 */
	void motor2_forward(int speed);
	/**
	 * Low level motor control, sets the motor#2 (right wheel) to the specified speed in backward direction.
	 * @param speed Speed of the motor, PWM value (0-255).
	 */
	void motor2_backward(int speed);
	/**
	 * Low level motor control, stops motor#2 (right wheel).
	 * @param speed Speed of the motor, PWM value (0-255).
	 */
	void motor2_stop();
	/**
	 * Starts both motors according to the PWM values specified. If the PWM value is negative, the motor in question will be started
	 * in backward direction.
	 * @param speed1 Speed of motor#1 (left motor) as set by the PWM value
	 * @param speed2 Speed of motor#2 (right motor) as set by the PWM value
	 */
	void motors_start( int speed1, int speed2 );
	/*
	 * Stops both motors
	 */
	void motors_stop();
	/**
	 * Low-level movement control function. It starts both motors according to initial power parameters then starts calling a set of supplied callbacks sequentally. Any of the
	 * callbacks may stop the movement if the callback's stop condition is satisfied or may update the power status of the motors.
	 * @param pwr1 Relative power of motor#1 (left) at the beginning of the movement.
	 * @param pwr2 Relative power of motor#2 (right) at the beginning of the movement.
	 * @param cbn Number of callback functions supplied in the cbs parameter
	 * @param cbs Function pointers of the callback functions
	 * @return Index of the callback that stopped the movement.
	 */
	int motors_run( int pwr1,int pwr2, int cbn, motors_run_callback cbs[] );
	static int motors_run_cb_timeout( TwoWDRobot *thisPtr, unsigned long startts, int *pwr1,int *pwr2 );
	static int motors_run_cb_adapt_direction( TwoWDRobot *thisPtr, unsigned long startts, int *pwr1,int *pwr2 );
	static int motors_run_cb_bump( TwoWDRobot *thisPtr, unsigned long startts, int *pwr1,int *pwr2 );
	static int motors_run_cb_turn( TwoWDRobot *thisPtr,unsigned long startts, int *pwr1,int *pwr2 );
	static int motors_run_cb_distance( TwoWDRobot *thisPtr,unsigned long startts, int *pwr1,int *pwr2 );
	
	/**
	 * The robot drives in a straight line. It is possible to specify the time or the distance of the movement. It is also possible
	 * to request obstacle detection.
	 * @param time_to_run Time of the movement, in millisec. If 0, the time of the movement is not specified.
	 * @param distance_to_run Distance of the movement as measured by the wheel rotation counter. If 0, the distance of the movement is not specified.
	 * @param pwr Motor power, expressed of percentage of maximum power (so 100 is the maximum power). The direction control logic adapts the motor power so that the
	 *            robot advances in straight line. This means that only one wheel is guaranteed to be powered with this motor power, the other motor may be driven
	 * 	      with less power if the robot's direction has to be corrected.
	 * @param bd  Obstacle detection, expressed in centimeters. If this value is nonzero, the proximity sensor will make a mesurement in every 100 msec and will stop the
	 *            robot if there is an object closer than bd centimeters.
	 * @return The reason, why the movement was stopped.
	 * 		- MOTOR_RUN_TIMEOUT - The timeout specified by the time_to_run parameter has expired
	 * 		- MOTOR_RUN_PROXIMITY - The proximity sensor detected an obstacle and stopped the movement
	 * 		- MOTOR_RUN_DISTANCE - The distance specified by the distance_to_run parameter has been reached
	 */
	int timed_straight_drive( unsigned long time_to_run, unsigned int distance_to_run, int pwr, int bd );
	/**
	 *  The robot turns. 
	 * @param turn_by_degree The angle of turning in degrees. Negative angles indicate turning left, positive angles mean turning right.
	 * @param pwr Motor power used at the beginning of the movement, percentage of the maximum power (so 100 is the maximum power). The function constantly adapts the
	 *        motor powers so at the end of the movement the power is considerably smaller than the power specified in this parameter.
	 * @return The reason why the movement was stopped
	 * 		- MOTOR_RUN_TIMEOUT - The timeout specified of 5 secs expired and the target degree was not reached
	 * 		- MOTOR_RUN_LIMITPARM - The target degree was reached
	 */
	int turn( float turn_by_degree, int pwr );

	
  private:
// ---------------------------- Gyro/accel section --------------------------------
	void init_gyroaccel();
	static float angle_diff( float a, float b );
	float reset_fifo();

// -------------------------- Proximity sensor ------------------------------------
	void init_proximity();

// -------------------------- Proximity servo -------------------------------------
	void init_proximity_servo();

// -------------------------- Wheel counter ---------------------------------------
	void init_wheel_counter();
	
// -------------------------- Motors section --------------------------------------
	void dump_calib_table();
	void init_motors();
	void reset_pid_memory();
	int motor_pid_controller( float error );
	int get_pwm_from_power_percentage( bool left_motor, int pwr_percentage );

  private:
// ---------------------------- Gyro/accel section --------------------------------- 
// Use the following global variables and access functions to help store the overall
// rotation angle of the sensor
	bool		dmpReady;
	uint16_t packetSize;    // expected DMP packet size (default is 42 bytes)
	unsigned long		gyro_last_timestamp;
	
// orientation/motion vars
	Quaternion q;           // [w, x, y, z]         quaternion container
	VectorFloat gravity;    // [x, y, z]            gravity vector
	float ypr[3];           // [yaw, pitch, roll]   yaw/pitch/roll container and gravity vector
	
// -------------------------- Proximity servo -------------------------------------
	Servo		proximityServo;

// -------------------------- Motors section --------------------------------------
	unsigned long 	run_timeout;
	float		direction_to_keep;
	int		pwr_to_keep;
	int		bump_distance;
	float		direction_to_reach;
	float		turn_angle;
	unsigned long	wheel_counter_to_reach;
	unsigned long		last_bump_check_timestamp;
	unsigned long		last_motor_control_timestamp;
	unsigned long		last_dir_reached_timestamp;
#ifdef DEBUG
	unsigned long		last_motor_debug_msg_timestamp;
#endif
#ifdef LIFE_SIGNAL
	unsigned long		life_signal_timestamp;
#endif
	motor_calib_array motor1_calib;
	motor_calib_array motor2_calib;
	float	pid_memory[PID_MEMORY_LEN];
	int	pid_memory_ptr;
};

#endif

#ifndef TWOWDROBOT_EEPROM_H
#define TWOWDROBOT_EEPROM_H

struct motor_calib_entry_s {
	uint8_t	pwm_value;
	uint8_t	power_percentage;
};
typedef struct motor_calib_entry_s	motor_calib_entry;


#define	EEPROM_BASE_ADDR	0x0

#define MOTOR_CALIB_BASE	EEPROM_BASE_ADDR
#define	MOTOR_CALIB_ARRAY_LEN	25
typedef motor_calib_entry       motor_calib_array[MOTOR_CALIB_ARRAY_LEN];
#define MOTOR_CALIB_1_BASE	MOTOR_CALIB_BASE
#define MOTOR_CALIB_1_LEN	sizeof(motor_calib_array)
#define MOTOR_CALIB_2_BASE	MOTOR_CALIB_1_BASE+MOTOR_CALIB_1_LEN
#define MOTOR_CALIB_2_LEN	sizeof(motor_calib_array)

#endif

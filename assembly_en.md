# Assembling the chassis

The assembly of the chassis is demonstrated in the images below.

![chassis1](images/chassis1.jpg)

![chassis1](images/chassis2.jpg)

![chassis1](images/chassis3.jpg)

The tyre patterns must be directed as shown in the image. It happens that the tyres do not arrive in pairs, in this case the tyre can be removed from the wheel and can be mounted in the correct position.

![chassis1](images/chassis4.jpg)

The part fixing the servo does not fit into its holder.

![chassis1](images/chassis5.jpg)


I have cut it into size with a cutting-plier. If you have better solution, share it!

![chassis1](images/chassis6.jpg)


![chassis1](images/chassis7.jpg)

The assembled servo/proximity detector mounted onto the robot.

![chassis1](images/chassis8.jpg)


# Connecting the electronic modules

The following modules have to be mounted onto the chassis.

* Arduino Uno
* Motor driver
* Gyroscope
* Speed sensor

The ideal position of the gyro is between the two wheels, the breakout board must be positioned according to the image below. The axis indications on the gyroscope board must be positioned
upward and toward the front of the board even though the location of the board itself can be chosen freely (e.g. above or under the chassis).

![Gyro direction](images/gyro_position.jpg)

The speed sensor has to be attached to the chassis so that the code wheel covers-passes through appropriately the light of the sensor. On the other hand the sensor must not
block the wheel's rotation. As the sensor is used only in case of straight-line motions, the sensor measures only the rotation of one of the two wheels.
The [twowdrobot_wheel](https://gitlab.com/paller/twowdrobot/tree/master/libraries/TwoWDRobot/examples/twowdrobot_wheel) test application assumes that the sensor measures
the left wheel (motor#1) otherwise it is not relevant, which wheel the sensor is attached to.

![Speed sensor1](images/speed1.jpg)

![Speed sensor2](images/speed2.jpg)


The referenced kit contains a board called "Arduino Sensor Shield v5.0". This component is optional, it merely provides better access to the Arduino Uno's connectors. If the
robot kit contains it, it is worth attaching it onto the top of the Arduino Uno.

The following connections have to be made among the modules.

## Battery holder

At least 7V battery pack is required. Robot kits normally come with 4xAA battery holder but 4 of the ordinary NiMh batteries (4x1.2V) do not provide enough voltage. I use a 2-pieces battery
holder with 2x3.7V AA-form Li-Ion batteries (type: 14500).


|Battery holder		     | Arduino Uno				      |
|----------------------      | -----------				      |
| Battery holder, - terminal | Arduino Uno power connector, external terminal |
| Battery holder, + terminal | Arduino Uno power connector, internal terminal |

## Motor controller

| Motor controller	| Arduino Uno 		|
| ------------  | ----------- 		|
|  GND		| GND         		|
| +5V		| Vin (__NOT__ +5V) 	|
| ENA		| Pin 5			|
| ENB		| Pin 6			|
| IN1		| Pin 8			|
| IN2		| Pin 9			|
| IN3		| Pin 10		|
| IN4		| Pin 11		|

| Motor controller  | Motors		|
| ------------  | ----------- 		|
| OUT1		| Left motor/1		|
| OUT2		| Left motor/2		|
| OUT3		| Right motor/1		|
| OUT4		| Right motor/2		|

Once the cabling has been finished, upload the [robot_motor_test3](https://gitlab.com/paller/twowdrobot/tree/master/libraries/TwoWDRobot/examples/robot_motor_test3) test program into the
Arduino Uno through the USB cable. Disconnect the USB cable, lift the robot (so that its wheels are not in contact with the ground), 
switch on the battery and you should see both wheels rotating forward with increasing then decreasing speed. Should any of the wheels turn backward, swap the appropriate OUT connectors
at the motor controller.

## Gyroscope

| Gyroscope	| Arduino Uno 		|
| ------------  | ----------- 		|
| GND		| GND			|
| VCC		| +5V			|
| SCL		| A5/SCL		|
| SDA		| A4/SDA		|
| INT		| Pin 3			|

## Servo

In my kit the servo already had its cables installed with the following color code:

| Servo		| Arduino Uno 		|
| ------------  | ----------- 		|
| Brown		| GND			|
| Red		| +5V			|
| Yellow	| Pin 12		|

## Proximity detector

| Proximity detector	| Arduino Uno 		|
| ----------------  	| ----------- 		|
| GND			| GND			|
| Vcc			| +5V			|
| Trig			| Pin 7			|
| Echo			| Pin 4			|

Upload the [twowdrobot_proximity](https://gitlab.com/paller/twowdrobot/tree/master/libraries/TwoWDRobot/examples/twowdrobot_proximity) test program into the Arduino Uno
through the USB cable and connect a serial adapter to the Tx pin of the Uno. After the cable is connected (USB alone can power this function), the servo must rotate into
0, 90 and 180 degrees in every 5 seconds and the distance measured by the proximity detector is emitted through the serial port.

## Speed sensor

| Sensor 		| Arduino Uno 		|
| ------  		| ----------- 		|
| GND			| GND			|
| VCC			| +5V			|
| DO			| Pin 2			|


Upload the [twowdrobot_wheel](https://gitlab.com/paller/twowdrobot/tree/master/libraries/TwoWDRobot/examples/twowdrobot_wheel) test program into the Arduino Uno
through the USB cable and connect a serial adapter to the Tx pin of the Uno.  Disconnect the USB cable, lift the robot (so that its wheels are not in contact with the ground), 
switch on the battery and check the output on the serial terminal. First the speed sensor is checked with stopped wheels. The result must be 0. Then the left wheel
starts spinning for 5 seconds and at the end the clicks counted by the speed measurement sensor is displayed. The result should be between 600-750 depending on the characteristics
of the motor.
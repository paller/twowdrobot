*[Click here for English version!](README_en.md)*

![2wdrobot](images/2wdrobot.jpg)

# Bevezető

Ez a projekt egy kétkerekű robot vezérlőszoftveréről szól. Maga a hardver olcsón megvásárolható többféle forrásból. A cél egy szoftverkönyvtár létrehozása, amellyel könnyen implementálhatók
robotalkalmazások.

[Itt egy rövid videó a robotról (ez a twowdrobot_turn példaprogram)](https://www.youtube.com/watch?v=XNFh8IUSGUQ). A robotot a fiam, Paller Boldizsár rakta össze.

[Olvasd a robotról szóló blogot!](http://littlechineserobot.blogspot.com/)


[A projektet a Budapest Makers' Meetup inspirálta](https://www.meetup.com/Budapest-Makers-Meetup/)


# Alkatrészek

* Kétkerekű robotalváz és motorok (hasznos keresőkifejezés az aliexpress.com-on: "2WD robot")
* Arduino Uno
* Arduino Sensor Shield v5.0 (nem kötelező, megkönnyíti a kábelek csatlakoztatását)
* L298N motorvezérlő (hasznos keresőkifejezés az aliexpress.com-on: "L298N motor driver board")
* MPU-6050 giroszkóp kártya (általában GY-521 típusjel alatt forgalmazzák).
* Optikai sebességmérő szenzor (hasznos keresőkifejezés az aliexpress.com-on: "Optical speed measuring sensor")

Például [ez a csomag](https://www.aliexpress.com/item/New-Avoidance-tracking-Motor-Smart-Robot-Car-Chassis-Kit-Speed-Encoder-Battery-Box-2WD-Ultrasonic-module/32587999240.html) 
tartalmaz a giroszkóp és a sebességmérő kivételével mindent a fenti listából. [A giroszkóp egy forrása ez](https://www.aliexpress.com/item/Electronic-Board-6DOF-MPU6050-Module-Gyroscope-And-3-for-Axis-Accelerometer-Sensor-Module-For-Arduino-3/32605525405.html) , 
a [sebességmérő pl. itt beszerezhető](https://www.aliexpress.com/item/1PCS-Speed-Measuring-Sensor-Counter-Motor-Test-Module-Groove-Type-Optical-Coupling-Module/32788770709.html) 
(hivatkozások érvényesek voltak 2017 április 11.-én). 

# A robot összeszerelése

[Összeszerelési útmutató](assembly.md)

# Installáció

* Nyisd meg a [projekt forrásainak a lapját](https://gitlab.com/paller/twowdrobot/tree/master).
* A jobb felső sarokban levő letöltés-ikonnal töltsd le a forrásokat ZIP formátumban.
* Csomagold ki a ZIP fájlt egy könyvtárba.
* A kicsomagolt fájlok közül válaszd ki a *libraries* könyvtárt és másold az Arduino könyvtáradba.
* Az Arduino környezetben az Include library/TwoWDRobot opcióval lehet a könyvtárt hozzáadni a projekthez.

# Kalibráció

A DC motorok meglehetősen különböző módon tudnak viselkedni. A szoftvercsomag tartalmaz egy kalibrációs programot, amelyik felfedezi a motorok karakterisztikáit és 
elmenti a mikrokontroller nem felejtő (EEPROM) memóriájába.

Használat:

* Töltsd fel a [twowdrobot_calib](https://gitlab.com/paller/twowdrobot/tree/master/libraries/TwoWDRobot/examples/twowdrobot_calib) programot az Arduino Uno-ba.
* Helyezd el a robotot egy nagyobb, akadálymentes területre. A kalibráció során a robot mozogni fog, kb. 1 négyzetméter akadálymentes területre van szükség.
* Kapcsold be a robotot. Kb. 20-30 mp szünet után a robot egyre növekvő sebességgel forogni kezd az egyik kereke körül, majd ezt megismétli a másik kerék körül.
* Ha a forgás megállt, a robotot kikapcsolhatod és feltöltheted a saját programjaidat. A kalibrációs adatok már el vannak mentve az Arduino Uno EEPROM-jába és a robotkönyvtár fel
  tudja használni azokat.


# Példaprogramok

A projekt tartalmaz néhány példaprogramot, amelyek installálás után az Arduino környezetben a File/Examples/TwoWDRobot menüpont alatt találhatóak. 

* *robot_motor_test3* A motorok tesztelésére való az alváz összeszerelése után, felpörgeti majd lepörgeti mindkét motort. Ellenőrizhető vele, hogy a kerekek egy irányba forognak-e és mindkét motor működik-e.
* *twowdrobot_motors* A motorok alapvető tesztje, egymás után előre-hátramenetbe kapcsolja a kerekeket.
* *twowdrobot_gyro* A giroszkóp tesztje, a soros kimeneten a robot elfordulási szögeit írja ki.
* *twowdrobot_proximity* A közelségérzékelő és az őt forgató szervó tesztje. A szervót 90 fokos szögekben elforgatja, közben a soros porton kijelzi a közelségérzékelő által mért távolságot.
* *twowdrobot_drive* A giroszkóp-vezérelt motorkorrekció tesztje, a robot előre-hátra mozog.
* *twowdrobot_calib* Motorkalibráló alkalmazás.
* *twowdrobot_turn* A giroszkóp-vezérelt motorkorrekció és fordulás tesztje, a robot előre halad, majd jobbra fordul 90 fokot. Akkor is fordul, ha a közelségérzékelő 20 cm-nél közelebbi akadályt jelez.
  Kalibráció után használandó.
* *twowdrobot_scan* A közelségérzékelő tesztje, körbeforgatja az ultrahangos szenzort, megméri a távolságokat és az eredményt kiírja a soros porton.
* *twowdrobot_patrol* Közelségérzékelő által vezérelt egyszerű robotnavigáció.

